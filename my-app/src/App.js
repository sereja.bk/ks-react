import React from 'react';
import CookiesManager from 'js-cookie';

import './App.css';

import LoginForm from "./Components/LoginForm";
import Navigator from './Components/Navigator';
import Header from './Components/Header';

import Dishes from './Components/Tables/Dishes';
import Cookers from './Components/Tables/Cookers';
import Ingredients from './Components/Tables/Ingredients';

class App extends React.Component {
  state = {
    isLoginned: false,
    token: undefined,
    name: "Гоглев Сергей Владимирович",
    activeWindow: 'record',
    viewId: undefined,
  }

  hadleLoginClick = () => {
    if (this.state.isLoginned === false) {
      this.setState({ isLoginned: true })
      CookiesManager.set('isLoginned', true);
    }
    else {
      CookiesManager.set('isLoginned', false);
      CookiesManager.set('token', undefined);
      this.setState({ isLoginned: false })
    }
  }

  changeWindow = (new_window) => {
    this.setState({ activeWindow: new_window })
  }

  setToken = (s_token) => {
    this.setState({ token: s_token })
    CookiesManager.set('token', s_token);
  }

  setView = (activeWindow, id) => {
    this.setState({ activeWindow: activeWindow, viewId: id })
  }

  render() {
    return (
      <>
        {this.state.isLoginned? <div >
          <Header onButtonClick={this.hadleLoginClick} name={this.state.name} />
          <div className='MainScreen'>
            <Navigator changeWindow={this.changeWindow} />
            {(() => {
              switch (this.state.activeWindow) {
                case 'cookers':
                  return (
                    <Cookers token={this.state.token} setView={this.setView} />
                  )
                case 'dishes':
                  return (
                    <Dishes token={this.state.token} setView={this.setView} />
                  )
                case 'ingredients':
                  return (
                    <Ingredients token={this.state.token} setView={this.setView} />
                  )
                default:
                  return (
                    <Cookers token={this.state.token} setView={this.setView} />
                  )
              }

            })()}
          </div>
        </div> : <LoginForm onButtonClick={this.hadleLoginClick} setToken={this.setToken} isLoginned={this.state.isLoginned} token={this.state.token} />}
      </>
    );
  }
}

export default App;
