import React from 'react';

import './Header.css'

class Header extends React.Component {
    handleClick = () => {
        const {onButtonClick} = this.props;
        onButtonClick();
    }

    render() {
        return (
            <div className='App-header'>
                <div className='header'>
                    {this.props.name}
                    <button onClick={this.handleClick} className='escapeBtn'>
                        Logout
                    </button>
                </div>
            </div> 
        )
    }
}

export default Header;