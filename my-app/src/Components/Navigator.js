import React from 'react';
import './Navigator.css'

import PeopleIcon from '@mui/icons-material/People';
import CookieIcon from '@mui/icons-material/Cookie';
import ListIcon from '@mui/icons-material/List';

const buttonArray = [
    {
        text: 'Все повары',
        icon: <PeopleIcon />,
        title: 'cookers'
    },
    {
        text: 'Все блюда',
        icon: <CookieIcon />,
        title: 'dishes'

    },
    {
        text: 'Все ингредиенты',
        icon: <ListIcon />,
        title: 'ingredients'
    }
]

class Navigator extends React.Component{
    render() {
        const {changeWindow} = this.props;
        return (
            <div className='navigator'>
                {buttonArray.map((button) => (
                    <div className='icon'>
                        {button.icon}
                        <button className='nav-button' onClick={changeWindow.bind(this, button.title)}>
                            {button.text}
                        </button>
                    </div>
                ))}
            </div>
        )        
    }
}

export default Navigator;
