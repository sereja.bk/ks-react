import React from 'react';

class Dishes extends React.Component {
    render() {
        return (
            <div className='Tables'>
                <div className='HeadTab'>
                    <h3>Блюда:</h3>
                </div>
                <div className='ListTab'>
                    <div>Салат "Цезарь"</div>
                    <div>Жаркое по-деревенски</div>
                    <div>Куриный беф-строганов</div>
                    <div>Макароны с мясом в томатном соусе</div>
                </div>
            </div>
        )
    }
}

export default Dishes;
