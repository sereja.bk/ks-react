import React from 'react';

class Ingredients extends React.Component{
    render() {
        return (
            <div className='Tables'>
                <div className='HeadTab'>
                    <h3>Ингредиенты:</h3>
                </div>
                <div className='ListTab'>
                    <div>Салат "Цезарь" - сухари</div>
                    <div>Салат "Цезарь" - лист-салат</div>
                    <div>Жаркое по-деревенски - картошка</div>
                    <div>Жаркое по-деревенски - говядина</div>
                    <div>Куриный беф-строганов - грудка курицы</div>
                    <div>Куриный беф-строганов - масло</div>
                    <div>Макароны с мясом в томатном соусе - спаггети</div>
                    <div>Макароны с мясом в томатном соусе - томатный соус</div>
                </div>
            </div>
        )
    }
}

export default Ingredients;
