import React from 'react';
import './Tables.css'

class Cookers extends React.Component{
    render() {
        return (
            <div className='Tables'>
                <div className='HeadTab'>
                    <h3>Повары:</h3>
                </div>
                <div className='ListTab'>
                    <div>Афанасьев Данил Александрович</div>
                    <div>Кудрявцев Александер Владимирович</div>
                    <div>Ефремова Наталия Андреева</div>
                    <div>Павлова Диана Григорьевна</div>
                </div>
            </div>
        )
    }
}

export default Cookers;
